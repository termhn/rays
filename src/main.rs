use image::{GenericImage,GenericImageView,ImageBuffer,RgbImage};
use rand::prelude::*;


#[derive(Clone,Debug,Copy)]
struct Camera {
        lower_left_corner : Vec3,
        horizontal : Vec3,
        vertical : Vec3,
        origin : Vec3,
}

impl Camera {
    fn new() -> Camera{
        Camera {
        lower_left_corner : Vec3::new(-2.0,-1.0,-1.0),
        horizontal : Vec3::new(4.0,0.0,0.0),
        vertical : Vec3::new(0.0,2.0,0.0),
        origin : Vec3::new(0.0,0.0,0.0),
        }
    }
}


#[derive(Clone,Debug,Copy)]
struct Vec3 {
    x:f32,
    y:f32,
    z:f32
}


impl Vec3 {
    fn new(x:f32,y:f32,z:f32) -> Vec3 {
        Vec3 {
            x,y,z
        }
    }
    // ? should we be generating another one or not?
    fn length(&self) -> f32{
        (self.x.powf(2.0)+self.y.powf(2.0)+ self.z.powf(2.0)).powf(0.5)
    }
    fn squared_length(&self) -> f32{
        self.x*self.x +
        self.y*self.y +
        self.z*self.z
    }
    fn unit(&self)  -> Vec3 {
        let k = 1.0/( self.length());
        Vec3 {
            z:self.z*k,
            x:self.x*k,
            y:self.y*k,
        }
    }
    fn make_unit(&mut self){
        let k = 1.0/( self.length());
        self.x = self.x*k;
        self.y = self.y*k;
        self.z = self.z*k;
    }
    // ! note that the operator overloading is something present in rust just unfamiliar
    fn subtract(&self,other:Vec3) ->  Vec3 {
        Vec3 {
            x:self.x -other.x ,
            y:self.y - other.y,
            z:self.z -  other.z
        }
    }
    fn div(&self,val:f32) -> Vec3 {
        Vec3 {
            x:self.x /val,
            y:self.y /val,
            z:self.z /val,
        }
    }
    fn multv(&self,v:&Vec3) -> Vec3 {
        Vec3 {
            x:self.x*v.x,
            z:self.z*v.z,
            y:self.y*v.y,
        }
    }
    fn mult(&self,f:f32)-> Vec3 {
        Vec3{
            x:f*self.x,
            y:f*self.y,
            z:f*self.z
        }

    }
    fn add(&self,other:Vec3) ->  Vec3 {
        Vec3 {
            x:self.x +other.x ,
            y:self.y + other.y,
            z:self.z +  other.z
        }
    }
}


// origin is A direction is B
struct Ray {
    A:Vec3,
    B:Vec3
}


impl Ray {
    fn new(A:Vec3,B:Vec3) -> Ray {
        Ray {
            A,
            B
        }
    }
    fn point_at_parameter(&self,t:f32) -> Vec3 {
        self.A.add( self.B.mult(t))
    }
}

trait Hittable {
    fn hit(&self,r:&Ray,t_min:f32,t_max:f32,rec:&mut Hit_record) -> bool;
}


struct Sphere{
    center:Vec3,
    r:f32
}

impl Hittable for Sphere {
    // contents of this were taken from the function circle_hit
    fn hit(&self,r:&Ray,t_min:f32,t_max:f32,rec:&mut Hit_record) -> bool{
        let oc = r.A.subtract(self.center);
        let a = dot(&r.B,&r.B);// b is the direction
        // I think these are off
        let b = 2.0*dot(&oc,&r.B);
        let c = dot(&oc,&oc) - self.r*self.r;
        let discriminant = b*b -4.0*a*c;
        if discriminant > 0.0 {
            let temp= (-b - discriminant.powf(0.5))/(2.0*a);// normally a 2 here but it will cancel
            if temp < t_max && temp > t_min {
                rec.t = temp;
                rec.p = r.point_at_parameter(rec.t);
                rec.normal = rec.p.subtract(self.center).div(self.r);
                return true
            }
            // try the other root
            let temp = (- b + discriminant.powf(0.5))/(2.0*a);
            if temp < t_max && temp > t_min {
                rec.t = temp;
                rec.p= r.point_at_parameter(rec.t);
                rec.normal= (rec.p.subtract(self.center)).div(self.r);
                return true
            }
        }
        return false
    }
}

struct HittableList {
    list:Vec<Box<dyn Hittable>>
}

impl Hittable for HittableList {
    fn hit(&self,r:&Ray,t_min:f32,t_max:f32,rec:&mut Hit_record) -> bool{
        let mut hit_anything = false;
        let mut closest_so_far = t_max;
        //?? what should the hit record start as?
        //it gets overwrited each time anyways, 
        let mut temp_rec = Hit_record {
            t:0.0,
            p:Vec3::new(0.0,0.0,0.0),
            normal:Vec3::new(0.0,0.0,0.0)
        };
        for i in 0..self.list.len() {
            let hit_ele = &self.list[i];
            if hit_ele.hit(&r,t_min,closest_so_far,&mut temp_rec) {
                hit_anything = true;
                closest_so_far = temp_rec.clone().t;
                *rec = temp_rec.clone();
            }
        }
        return hit_anything
    }
}

fn dot(v1:&Vec3,v2:&Vec3) -> f32 {
    v1.x*v2.x +
    v1.y*v2.y +
    v1.z*v2.z
}
#[derive(Clone)]
struct Hit_record {
    t:f32,
    p:Vec3,
    normal:Vec3
}

impl Hit_record {
    fn new() -> Hit_record {
        Hit_record {
            t:0.0,
            p:Vec3::new(0.0,0.0,0.0),
            normal:Vec3::new(0.0,0.0,0.0)
        }
    }
}

//?? can't pass world like this, should I use generics? I really wish rust would let me do this
//kind of thing
//

fn color(r:&Ray,world:&dyn Hittable) -> Vec3 {
    // B is teh direction of the ray
    let mut rec = Hit_record::new();
    if world.hit(&r,0.0,50000.0,&mut rec) {
        return Vec3::new(rec.normal.x+1.0, rec.normal.y+1.0, rec.normal.z+1.0).mult(0.5);
    } else {
        let u_dir = r.B.unit();
        let t = 0.5*(u_dir.y + 1.0);
        return Vec3::new(1.0,1.0,1.0).mult(1.0-t).add(Vec3::new(0.5,0.7,1.0).mult(t));
    }
}


fn main() {
    let nx = 200;
    let ny = 100;
    let mut im = ImageBuffer::new(nx,ny);
    let cam = Camera::new();
    let ns = 100;
    // create a list of the hittable obs
    let s1 = Sphere{
        center:Vec3::new(0.0,0.0,-1.0),
        r:0.5
    };
    let s2 = Sphere{
        center:Vec3::new(0.0,-100.5,-1.0),
        r:100.0
    };
    let world = HittableList{
        list:vec![Box::new(s1),Box::new(s2)]
    };
    let mut generator = thread_rng();
    for j in 0..ny {
        for i in 0..nx {
            // number of samples
            let mut acc = Vec3::new(0.0,0.0,0.0);
            for s in 0..ns {
                // bump the uv slightly to be sub pixel 
                let u = (i as f32 +generator.gen::<f32>()) / nx as f32;//generator gives back number between 0 1
                let v = (j as f32 +generator.gen::<f32>()) / ny as f32;
                // the mult is just a cheap way of forcing a clone =P
                let ray = Ray::new(cam.origin.clone(),cam.lower_left_corner.add( cam.horizontal.mult(u) ).add(cam.vertical.mult(v))); 

                let p = ray.point_at_parameter(2.0);
                let col = color(&ray,&world);
                acc = acc.add(col);
            }
            //average the color out
            acc = acc.div(ns as f32);
            let pix = image::Rgb([(acc.x*255.0) as u8,(acc.y*255.0) as u8,(acc.z*255.0) as u8]);
            im.put_pixel(i,j,pix);
        }
    }
    im.save("test.png").expect("saving problem");
}

